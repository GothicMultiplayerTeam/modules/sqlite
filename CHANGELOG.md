## Changelog

- Removed SQLite types constants
- Removed `SQLite3::open` method
- Removed `SQLite3::close` method
- Removed `SQLite3::isOpen` method
- Updated `SQLite3::execute` will now return array of columns (no need to use callback)
- Removed `SQLite3Stmt::getColumn` method
- Removed `SQLite3Stmt::getColumnCount` method

- Renamed `SQlite3.lastErrorCode` to `SQLite3.errcode` property
- Renamed `SQlite3.lastErrorMsg` to `SQLite3.errmsg` property

- Added SQLite open flags constants
- Added support for `blob` datatype
- Added optional argument in `SQLite3` constructor (flags)
- Added `SQLite3Stmt::getColumns`
- Added `SQLite3Stmt::bindValues`
- Added `SQLite3Stmt.columnCount` property
- Added `SQLite3Stmt.errcode` property
- Added `SQLite3Stmt.errmsg` property

- Updated `SQLite3Stmt::step` will now return `bool`
- Updated `SQLite3Stmt::reset` will now return `bool`
- Updated `SQLite3Stmt::clear` will now return nothing
- Updated `SQLite3Stmt::getColumns` will now return `table` where index is the name of the column