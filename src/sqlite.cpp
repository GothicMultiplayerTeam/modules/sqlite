#include "sqlite.h"
#include "sqlitestmt.h"

#include <memory>

SQLite3::SQLite3(sqlite3* db) :
	_handler(db)
{
}

SQLite3::~SQLite3()
{
	sqlite3_close(_handler);
}

int SQLite3::loadExtension(const char* libname) const
{
	SQChar* errMsg;

	int result = sqlite3_load_extension(_handler, libname, nullptr, &errMsg);
	if (result != SQLITE_OK)
	{
		Sqrat::string msg = "(SQLite3::loadExtension) ";
		msg += errMsg;
		sqlite3_free(errMsg);
		SqModule::Error(msg.c_str());
	}

	return result;
}

int SQLite3::enableLoadExtension(const bool value) const
{
	return sqlite3_enable_load_extension(_handler, value);
}

int SQLite3::busyTimeout(const int timeout) const
{
	return sqlite3_busy_timeout(_handler, timeout);
}

int SQLite3::changes() const
{
	return sqlite3_changes(_handler);
}

int SQLite3::lastInsertRowId() const
{
	return sqlite3_last_insert_rowid(_handler);
}

int SQLite3::errcode() const
{
	return sqlite3_errcode(_handler);
}

const SQChar* SQLite3::errmsg() const
{
	return sqlite3_errmsg(_handler);
}

SQInteger SQLite3::constructor(const HSQUIRRELVM vm)
{
	SQInteger top = sq_gettop(vm);
	if (top > 3)
		return sq_throwerror(vm, "wrong number of parameters");

	const SQChar* filename;
	sq_getstring(vm, 2, &filename);

	SQInteger flags = SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
	if (top == 3)
		sq_getinteger(vm, 3, &flags);

	sqlite3* db;
	int result = sqlite3_open_v2(filename, &db, flags, nullptr);

	if (result != SQLITE_OK)
		return sq_throwerror(vm, "(SQLite3::constructor): Cannot open database!");

	Sqrat::DefaultAllocator<SQLite3>::SetInstance(vm, 1, new SQLite3(db));
	return SQ_OK;
}

SQInteger SQLite3::execute(HSQUIRRELVM vm)
{
	SQLite3& self = *Sqrat::ClassType<SQLite3>::GetInstance(vm, 1);

	const SQChar* query;
	sq_getstring(vm, 2, &query);

	const char* left_over;
	sqlite3_mutex* mutex = sqlite3_db_mutex(self._handler);

	Sqrat::Array rows(vm);
	sq_pop(vm, 2);

	sqlite3_mutex_enter(mutex);
	while (query[0])
	{
		std::unique_ptr<SQLite3Stmt> stmt;

		try
		{
			stmt = std::unique_ptr<SQLite3Stmt>(self.prepare(query, &left_over));
		}
		catch (std::exception& e)
		{
			return sq_throwerror(vm, e.what());
		}

		if (!stmt)
		{
			query = left_over;
			continue;
		}

		Sqrat::PushVarR(vm, *stmt);

		for (bool step_status = stmt->step(); step_status == true; step_status = stmt->step())
		{
			if (SQ_FAILED(stmt->getColumns(vm)))
				return SQ_ERROR;

			HSQOBJECT obj;
			sq_getstackobj(vm, -1, &obj);

			rows.Append(Sqrat::Table(obj));
			sq_poptop(vm);
		}

		sq_poptop(vm);

		query = left_over;
		while ((query[0] >= 9 && query[0] <= 13) || query[0] == 32) ++query;
	}
	sqlite3_mutex_leave(mutex);

	Sqrat::PushVar(vm, rows);
	return 1;
}

SQInteger SQLite3::prepare(HSQUIRRELVM vm)
{
	SQLite3& self = *Sqrat::ClassType<SQLite3>::GetInstance(vm, 1);

	const SQChar* query;
	sq_getstring(vm, 2, &query);

	SQLite3Stmt* stmt;
	try
	{
		stmt = self.prepare(query);
	}
	catch (std::exception& e)
	{
		return sq_throwerror(vm, e.what());
	}

	Sqrat::ClassType<SQLite3Stmt>::PushInstance(vm, stmt, true);
	return 1;
}

SQLite3Stmt* SQLite3::prepare(const char* query, const char** left_over) const
{
	sqlite3_stmt* stmt;

	if (sqlite3_prepare_v2(_handler, query, -1, &stmt, left_over) != SQLITE_OK)
		throw std::runtime_error("(SQLite3::prepare) failed to prepare statement!");

	return new SQLite3Stmt(stmt);
}
