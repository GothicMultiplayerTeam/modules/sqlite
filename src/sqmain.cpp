#include "sqlite.h"
#include "sqlitestmt.h"

#include <sqapi.h>

extern "C" SQRESULT SQRAT_API sqmodule_load(HSQUIRRELVM vm, HSQAPI api)
{
	SqModule::Initialize(vm, api);

	Sqrat::RootTable rootTable(vm);

	/* squirreldoc (class)
	*
	* This is class for SQLite3 database management.
	*
	* @version	0.1
	* @side		shared
	* @name		SQLite3
	*
	*/
	rootTable.Bind("SQLite3", Sqrat::Class<SQLite3>(vm, "SQLite3")
		/* squirreldoc (constructor)
		*
		* @param	(string) filename path to the SQLite database, or **memory** to use in-memory database.
		* @param	(int) flags=SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE optional open flags. For more information see [Open flags](../../../shared-constants/open-flags).
		*
		*/
		.SquirrelFunc("constructor", &SQLite3::constructor, -2, ".si")

		/* squirreldoc (property)
		*
		* Represents number of database rows that were changed (or inserted or deleted) by the most recent SQL statement.
		*
		* @version	0.1
		* @readonly
		* @name     changes
		* @return   (int)
		*
		*/
		.Prop("changes", &SQLite3::changes)

		/* squirreldoc (property)
		*
		* Represents Row ID of the most recent INSERT into the database.
		*
		* @version	0.1
		* @readonly
		* @note		If no successful INSERTs into rowid tables have ever occurred on this database connection, then it will be 0.
		* @name     lastInsertRowId
		* @return   (int)
		*
		*/
		.Prop("lastInsertRowId", &SQLite3::lastInsertRowId)

		/* squirreldoc (property)
		*
		* Represents numeric result code of the most recent failed SQLite request.
		*
		* @version	0.3
		* @readonly
		* @name     errcode
		* @return   (int)
		*
		*/
		.Prop("errcode", &SQLite3::errcode)

		/* squirreldoc (property)
		*
		* Represents error message describing the most recent failed SQLite request.
		*
		* @version	0.3
		* @readonly
		* @name     errmsg
		* @return   (string)
		*
		*/
		.Prop("errmsg", &SQLite3::errmsg)

		/* squirreldoc (method)
		*
		* This method will set a busy handler that will sleep until the database is not locked or the timeout is reached.
		*
		* @version	0.1
		* @note		Passing a value less than or equal to zero, will turn off an already set timeout handler.
		* @name     busyTimeout
		* @param	(int) ms the milliseconds to sleep.
		* @return	(int) Result code.
		*
		*/
		.Func("busyTimeout", &SQLite3::busyTimeout)

		/* squirreldoc (method)
		*
		* This method will attempt to load an SQLite extension library.
		*
		* @version	0.1
		* @note		For security reasons, extension loading is turned off by default.<br>In order to use this functionality you'd must enable it first by using enableLoadExtension method.
		* @name     loadExtension
		* @param	(string) name the name of the library to load.
		* @return	(int) Result code.
		*
		*/
		.Func("loadExtension", &SQLite3::loadExtension)

		/* squirreldoc (method)
		*
		* This method enables or disables both the [SQLite3::loadExtension](#loadextension) and the SQL function load_extension().
		*
		* @version	0.1
		* @note		For security reasons, extension loading is turned off by default.
		* @name     enableLoadExtension
		* @param	(bool ) toggle
		* @return	(int) Result code.
		*
		*/
		.Func("enableLoadExtension", &SQLite3::enableLoadExtension)

		/* squirreldoc (method)
		*
		* This method prepares an SQL statement for execution.
		*
		* @version	0.1
		* @name     prepare
		* @param	(string) query the SQL query to prepare.
		* @return	(SQLite3Stmt) SQLite3Stmt object.
		*
		*/
		.SquirrelFunc("prepare", &SQLite3::prepare, 2, ".s")

		/* squirreldoc (method)
		*
		* This method returns multiple SQL queries result without the need to use prepared statement.
		*
		* @version	0.2
		* @note		Binded function will be invoked with 1 parameter which is a associative table.<br>Column names are indexes in that associative table.<br>All values in that table are strings.
		* @name     execute
		* @param	(string) query the SQL query to execute.
		* @return	(array)	array containing columns result(s).
		*
		*/
		.SquirrelFunc("execute", &SQLite3::execute, 2, ".s")
	);

	/* squirreldoc (class)
	*
	* This is class for prepared statements management.
	*
	* @version	0.1
	* @side		shared
	* @name		SQLite3Stmt
	*
	*/
	rootTable.Bind("SQLite3Stmt", Sqrat::Class<SQLite3Stmt>(vm, "SQLite3Stmt")
		/* squirreldoc (property)
		*
		* Represents a saved copy of the original SQL text used to create a prepared statement.
		*
		* @version	0.1
		* @readonly
		* @name     SQL
		* @return   (string)
		*
		*/
		.Prop("SQL", &SQLite3Stmt::SQL)

		/* squirreldoc (property)
		*
		* Represents the SQL text of prepared statement with bound parameters expanded.
		*
		* @version	0.1
		* @readonly
		* @name     expandedSQL
		* @return   (string)
		*
		*/
		.Prop("expandedSQL", &SQLite3Stmt::expandedSQL)

		/* squirreldoc (property)
		*
		* Represents numeric result code of the most recent failed SQLite request.
		*
		* @version	0.3
		* @readonly
		* @name     errcode
		* @return   (int)
		*
		*/
		.ConstVar("errcode", &SQLite3Stmt::errcode)

		/* squirreldoc (property)
		*
		* Represents error message describing the most recent failed SQLite request.
		*
		* @version	0.3
		* @readonly
		* @name     errmsg
		* @return   (string)
		*
		*/
		.Prop("errmsg", &SQLite3Stmt::errmsg)

		/* squirreldoc (property)
		*
		* Represents the number of parameters within the prepared statement.
		*
		* @version	0.3
		* @readonly
		* @name     paramCount
		* @return	(int)
		*
		*/
		.Prop("paramCount", &SQLite3Stmt::paramCount)

		/* squirreldoc (property)
		*
		* Represents the number of columns in the result set returned by the prepared statement.
		*
		* @version	0.3
		* @readonly
		* @name     columnCount
		* @return	(int)
		*
		*/
		.Prop("columnCount", &SQLite3Stmt::columnCount)

		/* squirreldoc (method)
		*
		* Represents if this SQL prepared statement query modifies the database.
		*
		* @version	0.3
		* @name		readOnly
		* @return	(bool)
		*
		*/
		.Func("readOnly", &SQLite3Stmt::readOnly)

		/* squirreldoc (method)
		*
		* This method evaluates An SQL statement and sets errcode to one of the following codes:  
		* - [SQLITE_BUSY](../../../shared-constants/result-codes/) database engine was unable to acquire the database locks it needs to do its job.  
		* - [SQLITE_DONE](../../../shared-constants/result-codes/) the statement has finished executing successfully.  
		* - [SQLITE_ERROR](../../../shared-constants/result-codes/) a run-time error (such as a constraint violation) has occurred.  
		* - [SQLITE_MISUSE](../../../shared-constants/result-codes/) this routine was called inappropriately.  
		* - [SQLITE_ROW](../../../shared-constants/result-codes/) another row of output is available.
		*
		* @version	0.1
		* @name     step
		* @return	(bool) returns `true` if next row can be queried, otherwise `false`.
		*
		*/
		.Func("step", &SQLite3Stmt::step)

		/* squirreldoc (method)
		*
		* This method resets the prepared statement to its state prior to execution.
		*
		* @version	0.1
		* @note If the most recent call to [SQLite3::step](../SQLite3/#loadextension) indicated an error, then this method will return an appropriate error code.
		* @note All bindings remain intact after reset.
		* @name     reset
		* @return	(bool) `true` if method succeeded, otherwise `false`.
		*
		*/
		.Func("reset", &SQLite3Stmt::reset)

		/* squirreldoc (method)
		*
		* This method will clear current bound parameters (sets them to `null`).
		*
		* @version	0.1
		* @name     clear
		*
		*/
		.Func("clear", &SQLite3Stmt::clear)

		/* squirreldoc (method)
		*
		* This method will bind the value of a parameter to a statement variable.
		*
		* @version	0.3
		* @note		Literals may be replaced by a parameter that matches one of following templates: <br>`?` <br>`?NNN` <br>`?VVV` <br>`@VVV` <br>`$VVV`
		* @note		`NNN` represents an integer literal, and `VVV` represents an alphanumeric identifier.
		* @name     bind
		* @param	(int|string) index index of the SQL parameter to be set.
		* @param	(int|float|bool|string|null) value value to bind to the parameter.
		* @return	(int) Result code.
		*
		*/
		.SquirrelFunc("bind", &SQLite3Stmt::bind, 3, ".")

		/* squirreldoc (method)
		*
		* This method will bind the values to a statement variables.
		*
		* @version	0.3
		* @name     bindValues
		* @param	(...) args the values to bind to statement.
		*
		*/
		.SquirrelFunc("bindValues", &SQLite3Stmt::bindValues)

		/* squirreldoc (method)
		*
		* This method gets information about columns of the current result row of a query.
		*
		* @version	0.2
		* @name     getColumns
		* @return	(table) Table containing all of the columns of the current result row of a query where index is the name of the column.
		*
		*/
		.SquirrelFunc("getColumns", &SQLite3Stmt::getColumns, 1, ".")
	);

	Sqrat::ConstTable(vm)
		/* squirreldoc (const)
		*
		* Represents SQLite3 version
		*
		* @category General
		* @side		shared
		* @name		SQLITE_VERSION
		*
		*/
		.Const("SQLITE_VERSION", SQLITE_VERSION)

		/* squirreldoc (const)
		*
		* Represents opening database in read only mode.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_READONLY
		*
		*/
		.Const("SQLITE_OPEN_READONLY", SQLITE_OPEN_READONLY)

		/* squirreldoc (const)
		*
		* Represents opening database in read/write mode.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_READWRITE
		*
		*/
		.Const("SQLITE_OPEN_READWRITE", SQLITE_OPEN_READWRITE)

		/* squirreldoc (const)
		*
		* Represents creating database file during opening if it doesn't exists.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_CREATE
		*
		*/
		.Const("SQLITE_OPEN_CREATE", SQLITE_OPEN_CREATE)

		/* squirreldoc (const)
		*
		* Represents automatic deletion of database file when SQLite3 object gets deleted.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_DELETEONCLOSE
		*
		*/
		.Const("SQLITE_OPEN_DELETEONCLOSE", SQLITE_OPEN_DELETEONCLOSE)

		/* squirreldoc (const)
		*
		* Represents opening database in exclusive mode (other applications cannot access this database file).
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_EXCLUSIVE
		*
		*/
		.Const("SQLITE_OPEN_EXCLUSIVE", SQLITE_OPEN_EXCLUSIVE)

		/* squirreldoc (const)
		*
		* Represents opening database with autoproxy feature.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_AUTOPROXY
		*
		*/
		.Const("SQLITE_OPEN_AUTOPROXY", SQLITE_OPEN_AUTOPROXY)

		/* squirreldoc (const)
		*
		* Represents using URI schema in filename argument during opening database.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_URI
		*
		*/
		.Const("SQLITE_OPEN_URI", SQLITE_OPEN_URI)

		/* squirreldoc (const)
		*
		* Represents creating database in RAM memory during opening, NOTE! you need to load the database data separately.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_MEMORY
		*
		*/
		.Const("SQLITE_OPEN_MEMORY", SQLITE_OPEN_MEMORY)

		/* squirreldoc (const)
		*
		* Represents opening database in persistent mode (all of the data is retained).
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_MAIN_DB
		*
		*/
		.Const("SQLITE_OPEN_MAIN_DB", SQLITE_OPEN_MAIN_DB)

		/* squirreldoc (const)
		*
		* Represents opening database in temporary mode (all of the data gets deleted when SQLite3 is closed).
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_TEMP_DB
		*
		*/
		.Const("SQLITE_OPEN_TEMP_DB", SQLITE_OPEN_TEMP_DB)

		/* squirreldoc (const)
		*
		* Represents opening database in transient mode (all of the data gets deleted after single SQL query).
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_TRANSIENT_DB
		*
		*/
		.Const("SQLITE_OPEN_TRANSIENT_DB", SQLITE_OPEN_TRANSIENT_DB)

		/* squirreldoc (const)
		*
		* Represents opening the main journal file associated with the database. The journal file is used for rollback and ensuring atomic transactions.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_MAIN_JOURNAL
		*
		*/
		.Const("SQLITE_OPEN_MAIN_JOURNAL", SQLITE_OPEN_MAIN_JOURNAL)

		/* squirreldoc (const)
		*
		* Represents opening a temporary journal file used for transactions in a temporary database.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_TEMP_JOURNAL
		*
		*/
		.Const("SQLITE_OPEN_TEMP_JOURNAL", SQLITE_OPEN_TEMP_JOURNAL)

		/* squirreldoc (const)
		*
		* Represents opening a sub-journal file, which is used in nested transactions to manage savepoints.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_SUBJOURNAL
		*
		*/
		.Const("SQLITE_OPEN_SUBJOURNAL", SQLITE_OPEN_SUBJOURNAL)

		/* squirreldoc (const)
		*
		* Represents opening a super-journal file, which is used in more complex transactions, often involving multiple databases.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_SUPER_JOURNAL
		*
		*/
		.Const("SQLITE_OPEN_SUPER_JOURNAL", SQLITE_OPEN_SUPER_JOURNAL)

		/* squirreldoc (const)
		*
		* Represents opening database with no thread safety features for one database connection.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_NOMUTEX
		*
		*/
		.Const("SQLITE_OPEN_NOMUTEX", SQLITE_OPEN_NOMUTEX)

		/* squirreldoc (const)
		*
		* Represents opening database with thread safety features that allows manipulating db from multiple threads.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_FULLMUTEX
		*
		*/
		.Const("SQLITE_OPEN_FULLMUTEX", SQLITE_OPEN_FULLMUTEX)

		/* squirreldoc (const)
		*
		* Represents opening database with shared cache, it means that database connection will use shared cache across all connections.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_SHAREDCACHE
		*
		*/
		.Const("SQLITE_OPEN_SHAREDCACHE", SQLITE_OPEN_SHAREDCACHE)

		/* squirreldoc (const)
		*
		* Represents opening database with private cache, it means that database connection will use it's own cache not shared with other connections.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_PRIVATECACHE
		*
		*/
		.Const("SQLITE_OPEN_PRIVATECACHE", SQLITE_OPEN_PRIVATECACHE)

		/* squirreldoc (const)
		*
		* Represents opening database with Write Ahead Logging feature.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_WAL
		*
		*/
		.Const("SQLITE_OPEN_WAL", SQLITE_OPEN_WAL)

		/* squirreldoc (const)
		*
		* Represents preventing opening database file that is a symlink due to security reasons.
		*
		* @category Open flags
		* @side		shared
		* @name		SQLITE_OPEN_NOFOLLOW
		*
		*/
		.Const("SQLITE_OPEN_NOFOLLOW", SQLITE_OPEN_NOFOLLOW)

		/* squirreldoc (const)
		* 
		* Represents result code that indicates that an operation has completed.  
		* This result code is most commonly seen as a return value from [SQLite3Stmt::step](../../shared-classes/general/SQLite3Stmt/#step) indicating that the SQL statement has run to completion.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_DONE
		*
		*/
		.Const("SQLITE_DONE", SQLITE_DONE)

		/* squirreldoc (const)
		*
		* Represents result code that indicates an internal malfunction.  
		* In a working version of SQLite, an application should never see this result code.  
		* If application does encounter this result code, it shows that there is a bug in the database engine.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_INTERNAL
		*
		*/
		.Const("SQLITE_INTERNAL", SQLITE_INTERNAL)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that the requested access mode for a newly created database could not be provided.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_PERM
		*
		*/
		.Const("SQLITE_PERM", SQLITE_PERM)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that an operation was aborted prior to completion, usually be application request.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_ABORT
		*
		*/
		.Const("SQLITE_ABORT", SQLITE_ABORT)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that a write operation could not continue because of a conflict within the same database connection or a conflict with a different database connection that uses a shared cache.  
		* For example, a DROP TABLE statement cannot be run while another thread is reading from that table on the same database connection because dropping the table would delete the table out from under the concurrent reader.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_LOCKED
		*
		*/
		.Const("SQLITE_LOCKED", SQLITE_LOCKED)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that SQLite was unable to allocate all the memory it needed to complete the operation.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_NOMEM
		*
		*/
		.Const("SQLITE_NOMEM", SQLITE_NOMEM)

		/* squirreldoc (const)
		*
		* Represents result code that is returned when an attempt is made to alter some data for which the current database connection does not have write permission.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_READONLY
		*
		*/
		.Const("SQLITE_READONLY", SQLITE_READONLY)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that an operation was interrupted.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_INTERRUPT
		*
		*/
		.Const("SQLITE_INTERRUPT", SQLITE_INTERRUPT)

		/* squirreldoc (const)
		*
		* Represents result code that says that the operation could not finish because the operating system reported an I/O error.  
		* A full disk drive will normally give an SQLITE_FULL error rather than an SQLITE_IOERR error.  
		* There are many different extended result codes for I/O errors that identify the specific I/O operation that failed.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_IOERR
		*
		*/
		.Const("SQLITE_IOERR", SQLITE_IOERR)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that the database file has been corrupted.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_CORRUPT
		*
		*/
		.Const("SQLITE_CORRUPT", SQLITE_CORRUPT)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that a write could not complete because the disk is full.  
		* Note that this error can occur when trying to write information into the main database file, or it can also occur when writing into temporary disk files.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_FULL
		*
		*/
		.Const("SQLITE_FULL", SQLITE_FULL)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that SQLite was unable to open a file.  
		* The file in question might be a primary database file or one of several temporary disk files.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_CANTOPEN
		*
		*/
		.Const("SQLITE_CANTOPEN", SQLITE_CANTOPEN)

		/* squirreldoc (const)
		*
		* Represents result code that indicates a problem with the file locking protocol used by SQLite.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_PROTOCOL
		*
		*/
		.Const("SQLITE_PROTOCOL", SQLITE_PROTOCOL)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that the database schema has changed.  
		* This result code can be returned from [SQLite3Stmt::step](../../shared-classes/general/SQLite3Stmt/#step) for a prepared statement that was generated using [SQLite3::prepare](../../shared-classes/general/SQLite3/#prepare)  
		* If the database schema was changed by some other process in between the time that the statement was prepared and the time the statement was run, this error can result.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_SCHEMA
		*
		*/
		.Const("SQLITE_SCHEMA", SQLITE_SCHEMA)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that a string or BLOB was too large.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_TOOBIG
		*
		*/
		.Const("SQLITE_TOOBIG", SQLITE_TOOBIG)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that an SQL constraint violation occurred while trying to process an SQL statement.  
		* Additional information about the failed constraint can be found by consulting the accompanying error message (returned via [SQLite3::lastErrorMsg](../../shared-classes/general/SQLite3/#int-lasterrorcode-read-only)) or by looking at the extended error code.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_CONSTRAINT
		*
		*/
		.Const("SQLITE_CONSTRAINT", SQLITE_CONSTRAINT)

		/* squirreldoc (const)
		*
		* Represents result code that indicates a datatype mismatch.  
		* SQLite is normally very forgiving about mismatches between the type of a value and the declared type of the container in which that value is to be stored.  
		* For example, SQLite allows the application to store a large BLOB in a column with a declared type of BOOLEAN. But in a few cases, SQLite is strict about types.  
		* This result code is returned in those few cases when the types do not match.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_MISMATCH
		*
		*/
		.Const("SQLITE_MISMATCH", SQLITE_MISMATCH)

		/* squirreldoc (const)
		*
		* Represents result code that might be returned if the application uses any SQLite interface in a way that is undefined or unsupported.  
		* For example, using a prepared statement after that prepared statement has been finalized might result in an `SQLITE_MISUSE` error.  
		* SQLite tries to detect misuse and report the misuse using this result code. However, there is no guarantee that the detection of misuse will be successful.  
		* Misuse detection is probabilistic. Applications should never depend on an `SQLITE_MISUSE` return value.  
		* If SQLite ever returns `SQLITE_MISUSE` from any interface, that means that the application is incorrectly coded and needs to be fixed.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_MISUSE
		*
		*/
		.Const("SQLITE_MISUSE", SQLITE_MISUSE)

		/* squirreldoc (const)
		*
		* Represents result code that can be returned on systems that do not support large files when the database grows to be larger than what the filesystem can handle.  
		* `NOLFS` stands for `NO Large File Support`.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_NOLFS
		*
		*/
		.Const("SQLITE_NOLFS", SQLITE_NOLFS)

		/* squirreldoc (const)
		*
		* Represents result code that is returned when the authorizer callback indicates that an SQL statement being prepared is not authorized.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_AUTH
		*
		*/
		.Const("SQLITE_AUTH", SQLITE_AUTH)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that the operation was successful and that there were no errors.  
		* Most other result codes indicate an error.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_OK
		*
		*/
		.Const("SQLITE_OK", SQLITE_OK)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that the database file could not be written (or in some cases read) because of concurrent activity by some other database connection, usually a database connection in a separate process.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_BUSY
		*
		*/
		.Const("SQLITE_BUSY", SQLITE_BUSY)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that the parameter number argument to one of the [SQLite3Stmt::bindValue](../../shared-classes/general/SQLite3Stmt/#bindValue) routines or the column number in one of the [SQLite3Stmt::getColumn](../../shared-classes/general/SQLite3Stmt/#getcolumn) routines is out of range.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_RANGE
		*
		*/
		.Const("SQLITE_RANGE", SQLITE_RANGE)

		/* squirreldoc (const)
		*
		* Represents result code that indicates that file being opened does not appear to be an SQLite database file.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_NOTADB
		*
		*/
		.Const("SQLITE_NOTADB", SQLITE_NOTADB)

		/* squirreldoc (const)
		*
		* Represents result code returned by [SQLite3Stmt::step](../../shared-classes/general/SQLite3Stmt/#step) that indicates that another row of output is available.
		*
		* @category Result codes
		* @side		shared
		* @name		SQLITE_ROW
		*
		*/
		.Const("SQLITE_ROW", SQLITE_ROW)
	;

	return SQ_OK;
}
