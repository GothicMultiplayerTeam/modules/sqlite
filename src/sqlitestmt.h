#pragma once

#include <sqapi.h>
#include <sqlite3.h>
#include <variant>

class SQLite3Stmt {
public:
	SQLite3Stmt(SQUserPointer stmt);
	~SQLite3Stmt();

	const char* SQL() const;
	const char* expandedSQL() const;
	int errcode = SQLITE_OK;
	const char* errmsg() const;
	int paramCount() const;
	int columnCount() const;
	bool readOnly() const;

	bool step();
	bool reset();
	void clear();

	static SQInteger bind(HSQUIRRELVM vm);
	static SQInteger bindValues(HSQUIRRELVM vm);
	static SQInteger getColumns(HSQUIRRELVM vm);

private:
	using IndexVariant = std::variant<SQInteger, const SQChar*>;
	using Blob = std::pair<SQUserPointer, SQInteger>;
	using ValueVariant = std::variant<SQInteger, SQFloat, const SQChar*, Blob, nullptr_t>;

	static SQInteger getValueVariant(HSQUIRRELVM vm, ValueVariant& value, HSQOBJECT& arg_value);
	bool bindValue(IndexVariant index, ValueVariant value);

	sqlite3_stmt *_stmt;
};