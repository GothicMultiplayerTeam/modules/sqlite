#include "sqlitestmt.h"
#include <vector>

SQLite3Stmt::SQLite3Stmt(SQUserPointer stmt)
{
	_stmt = reinterpret_cast<sqlite3_stmt*>(stmt);
}

SQLite3Stmt::~SQLite3Stmt()
{
	sqlite3_finalize(_stmt);
}

const char* SQLite3Stmt::errmsg() const
{
	return sqlite3_errstr(errcode);
}

int SQLite3Stmt::paramCount() const
{
	return sqlite3_bind_parameter_count(_stmt);
}

int SQLite3Stmt::columnCount() const
{
	return sqlite3_column_count(_stmt);
}

bool SQLite3Stmt::readOnly() const
{
	return sqlite3_stmt_readonly(_stmt);
}

const char* SQLite3Stmt::SQL() const
{
	return sqlite3_sql(_stmt);
}

const char* SQLite3Stmt::expandedSQL() const
{
	return sqlite3_expanded_sql(_stmt);
}

bool SQLite3Stmt::step()
{
	errcode = sqlite3_step(_stmt);
	return errcode == SQLITE_ROW;
}

bool SQLite3Stmt::reset()
{
	errcode = sqlite3_reset(_stmt);
	return errcode == SQLITE_OK;
}

void SQLite3Stmt::clear()
{
	sqlite3_clear_bindings(_stmt);
}

SQInteger SQLite3Stmt::getValueVariant(HSQUIRRELVM vm, ValueVariant& value, HSQOBJECT& arg_value)
{
	switch (arg_value._type)
	{
		case OT_INTEGER:
		case OT_BOOL:
		{
			value = arg_value._unVal.nInteger;
			return SQ_OK;
		}

		case OT_FLOAT:
		{
			value = arg_value._unVal.fFloat;
			return SQ_OK;
		}

		case OT_STRING:
		{
			sq_pushobject(vm, arg_value);
			const SQChar* valueText;
			sq_getstring(vm, -1, &valueText);
			value = valueText;
			sq_poptop(vm);
			return SQ_OK;
		}

		case OT_NULL:
		{
			value = nullptr;
			return SQ_OK;
		}

		case OT_INSTANCE:
		{
			sq_pushobject(vm, arg_value);
			sq_typeof(vm, -1);

			const SQChar* type;
			sq_getstring(vm, -1, &type);

			if (strcmp(type, "blob") == 0)
			{
				sq_poptop(vm);

				SQUserPointer blob;
				sqstd_getblob(vm, -1, &blob);

				SQInteger blob_size = sqstd_getblobsize(vm, -1);

				value = std::make_pair(blob, blob_size);
				sq_poptop(vm);

				return SQ_OK;
			}
		}
	}

	return sq_throwerror(vm, "wrong type of parameter 2, expecting 'integer|float|string|blob|bool|null'");
}

SQInteger SQLite3Stmt::bind(HSQUIRRELVM vm)
{
	SQLite3Stmt& self = *Sqrat::ClassType<SQLite3Stmt>::GetInstance(vm, 1);
	IndexVariant index;

	switch (sq_gettype(vm, 2))
	{
		case OT_INTEGER:
		{
			SQInteger arg_index;
			sq_getinteger(vm, 2, &arg_index);
			index = arg_index;
			break;
		}

		case OT_STRING:
		{
			const SQChar* textIndex;
			sq_getstring(vm, 2, &textIndex);
			index = textIndex;
			break;
		}

		default:
			return sq_throwerror(vm, "(SQLite3Stmt::bindValue) wrong type of parameter 0, expecting 'integer|string'");
	}

	HSQOBJECT arg;
	sq_getstackobj(vm, 3, &arg);

	ValueVariant value;
	if (SQ_FAILED(getValueVariant(vm, value, arg)))
		return SQ_ERROR;

	if (!self.bindValue(index, value)) {
		sq_pushbool(vm, SQFalse);
		return 1;
	}

	sq_pushbool(vm, SQTrue);
	return 1;
}

SQInteger SQLite3Stmt::bindValues(HSQUIRRELVM vm)
{
	SQLite3Stmt& self = *Sqrat::ClassType<SQLite3Stmt>::GetInstance(vm, 1);

	SQInteger param_count = self.paramCount();
	const SQInteger top = sq_gettop(vm);

	if (SQInteger args_count = top - 1; param_count != args_count)
		return sqstd_throwerrorf(vm, "(SQLite3Stmt::bindValues) wrong number of parameters, expected %d, passed %d", param_count, args_count);

	for (SQInteger i = top; i > 1; --i)
	{
		HSQOBJECT arg;
		sq_getstackobj(vm, i, &arg);

		ValueVariant variant;
		if (getValueVariant(vm, variant, arg) == SQ_ERROR)
			return SQ_ERROR;

		if (!self.bindValue(i - 1, variant)) {
			sq_pushbool(vm, SQFalse);
			return 1;
		}
	}

	sq_pushbool(vm, SQTrue);
	return 1;
}

SQInteger SQLite3Stmt::getColumns(HSQUIRRELVM vm)
{
	SQLite3Stmt& self = *Sqrat::ClassType<SQLite3Stmt>::GetInstance(vm, 1);

	int column_count = sqlite3_column_count(self._stmt);
	Sqrat::Table result(vm);

	for (int i = 0; i != column_count; ++i)
	{
		const char* column_name = sqlite3_column_name(self._stmt, i);

		switch (sqlite3_column_type(self._stmt, i))
		{
			case SQLITE_INTEGER:
	#ifndef _SQ64
				result.SetValue(column_name, sqlite3_column_int(self._stmt, i));
	#else
				result.SetValue(i, sqlite3_column_int64(self._stmt, i));
	#endif
				break;

			case SQLITE_FLOAT:
				result.SetValue(column_name, sqlite3_column_double(self._stmt, i));
				break;

			case SQLITE3_TEXT:
				result.SetValue(column_name, reinterpret_cast<const char*>(sqlite3_column_text(self._stmt, i)));
				break;

			case SQLITE_BLOB:
			{
				const int blob_size = sqlite3_column_bytes(self._stmt, i);

				SQUserPointer sq_blob = sqstd_createblob(vm, blob_size);
				if (sq_blob == nullptr)
					return sq_throwerror(vm, "(SQLite3Stmt::getColumns) cannot create sq blob!");

				const void* sqlite_blob = sqlite3_column_blob(self._stmt, i);
				if (sqlite_blob == nullptr)
					return sq_throwerror(vm, "(SQLite3Stmt::getColumns) cannot create sqlite blob!");

				memcpy(sq_blob, sqlite_blob, blob_size);

				HSQOBJECT obj;
				sq_getstackobj(vm, sq_gettop(vm), &obj);

				result.SetValue(column_name, Sqrat::Object(obj));
				break;
			}

			case SQLITE_NULL:
				result.SetValue(column_name, nullptr);
				break;
			}
	}

	Sqrat::PushVar(vm, result);
	return 1;
}

bool SQLite3Stmt::bindValue(IndexVariant index, ValueVariant value)
{
	struct IndexCallable
	{
		int operator()(sqlite3_stmt*, SQInteger index) const { return index; }
		int operator()(sqlite3_stmt* stmt, const SQChar* index) const { return sqlite3_bind_parameter_index(stmt, index); }
	} index_callable;

	struct ValueCallable
	{
		int operator()(sqlite3_stmt* stmt, int index, SQInteger value) const { return sqlite3_bind_int(stmt, index, value); }
		int operator()(sqlite3_stmt* stmt, int index, SQFloat value) const { return sqlite3_bind_double(stmt, index, value); }
		int operator()(sqlite3_stmt* stmt, int index, const SQChar* value) const { return sqlite3_bind_text(stmt, index, value, strlen(value), nullptr); }
		int operator()(sqlite3_stmt* stmt, int index, Blob value) const
		{
			return sqlite3_bind_blob(stmt, index, value.first, value.second, nullptr);
		}
		int operator()(sqlite3_stmt* stmt, int index, nullptr_t value) const { return sqlite3_bind_null(stmt, index); }
	} value_callable;

	int idx = std::visit([this, &index_callable](auto&& arg) { return index_callable(_stmt, arg); }, index);
	errcode = std::visit([this, &value_callable, &idx](auto&& arg) {return value_callable(_stmt, idx, arg); }, value);

	return errcode == SQLITE_OK;
}
