#pragma once

#include <sqapi.h>

struct sqlite3;
struct sqlite3_stmt;
class SQLite3Stmt;

class SQLite3 {
public:
	SQLite3(sqlite3 *db);
	~SQLite3();

	int loadExtension(const char* libname) const;
	int enableLoadExtension(bool value) const;
	int busyTimeout(int timeout) const;
	int changes() const;
	int lastInsertRowId() const;
	int errcode() const;
	const char* errmsg() const;

	static SQInteger constructor(HSQUIRRELVM);
	static SQInteger execute(HSQUIRRELVM);
	static SQInteger prepare(HSQUIRRELVM);

private:
	SQLite3Stmt* prepare(const char* query, const char** left_over = nullptr) const;

	sqlite3* _handler;
};
