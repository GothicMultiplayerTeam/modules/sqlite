# SQLite

## Introduction

SQLite module binds a third-party SQLite3 library and provides squirrel API to access data from SQLite databases.  
SQLite is a perfect alternative for MySQL when you want to use SQL databases but without a server (stored in file(s)).

The project uses amalgamated sources for SQLite connector that were downloaded from [here](https://www.sqlite.org/download.html)

## Documentation

https://gothicmultiplayerteam.gitlab.io/modules/SQLite