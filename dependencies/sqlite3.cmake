add_library(sqlite3 STATIC)

target_sources(sqlite3
	PRIVATE
		"sqlite3/sqlite3.h"
		"sqlite3/sqlite3.c"
)

target_include_directories(sqlite3
	PUBLIC
		"${CMAKE_CURRENT_SOURCE_DIR}/sqlite3/"
)

target_compile_options(sqlite3
	PRIVATE
		$<$<PLATFORM_ID:Linux>:-fPIC>
)