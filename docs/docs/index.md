# Home

Welcome to the official documentation for SQLite3 module. You can find here important information about how to use that module.

Important links:  
- [bug tracker](https://gitlab.com/GothicMultiplayerTeam/modules/sqlite/-/issues)  